import Vue from 'vue'
import Vuex from 'vuex'
import { uuid } from 'vue-uuid';

import api from '@/services/request';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    favoritCourseList: localStorage.getItem('savedData') || '[]',
    allCourses: []
  },
  mutations: {
    updateFavoriteCourseState(state, payload) {
      state.favoritCourseList = payload;
    },
    getAllCourses(state, payload) {
      state.allCourses = payload;
    }
  },
  actions: {
    addFavoritCourse({ commit }, payload) {
      let updateList = localStorage.getItem('savedData') || [];
      updateList = JSON.parse(updateList);
      if(updateList ) {
        payload.forEach(e => { 
          updateList.push(e)
        });
        localStorage.setItem("savedData", JSON.stringify(updateList));
        commit('updateFavoriteCourseState', JSON.stringify(updateList));
      } else {
        localStorage.setItem("savedData", JSON.stringify(payload));
        commit('updateFavoriteCourseState', JSON.stringify(payload));
      }
    },
    removeFavoriteCourse({ commit }, payload) {
      let updateList = localStorage.getItem('savedData');
      updateList = JSON.parse(updateList);

      if(updateList ) {
        updateList = updateList.filter(e => e.uuid !== payload);        
        localStorage.setItem("savedData", JSON.stringify(updateList));
        commit('getAllCourses', JSON.stringify(updateList));
      } else {
        localStorage.setItem("savedData", JSON.stringify(updateList));
      }    
    },
    async getAllCourses() {
      const { data } =  await api.$http.get('/scholarships');     
      const courses = data.map(element => {
        element.uuid = uuid.v1();
        return element;        
      });
      this.commit('getAllCourses', courses);
    }
  },
  getters: {
    getFavoriteCourses(state) {
      let currentList = state.favoritCourseList || '[]';
      return JSON.parse(currentList);
    },
  }
})
