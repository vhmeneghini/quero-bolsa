# Quero educação Test front-end

## Development

Use ```yarn``` e o comando desejado.

| Comando | Descrição |
|--|--|
| yarn install | Instalar todas as dependências
| yarn serve | Rodar o servidor com hot reload em desenvolvimento
| yarn build | Construir arquivo minificado para produção
| yarn lint --fix | Verificar e corrigir erros fora do padrão


### Considerações/ Pontos de melhoras 

Por conta do meu trabalho atual não consegui dedicar o tempo que julguei necessario para completar todos os requisitos. Então separei possiveis pontos que poderiam ser melhorados (:

- Componente que é utilizado como modal poderia ser generico e reutilizado em outros elementos atraves de slots
- Para cobertura de test seria interessante utilizar o Vue test utils (Lib que ajuda a preparar testes unitarios dos componentes)
- Poderia criar um utils contendo uma Classe generica para realizar a manipulação de listas


### Css Architecture // tyle guide de Javascript;

Durante toda o desenvolvimento, foi utilizado o BEM (Blocks, Elements and Modifiers)
http://getbem.com/introduction/

Para o java script foi utilizado o https://vuejs.org/v2/style-guide/


## Demo
https://cocky-jang-68f67c.netlify.com/